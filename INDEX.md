# sort

Sort the contents of a text file, optionally using the NLS collate table

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## SORT.LSM

<table>
<tr><td>title</td><td>sort</td></tr>
<tr><td>version</td><td>1.5.1a</td></tr>
<tr><td>entered&nbsp;date</td><td>2016-11-13</td></tr>
<tr><td>description</td><td>Sort the contents of a text file</td></tr>
<tr><td>summary</td><td>Sort the contents of a text file, optionally using the NLS collate table</td></tr>
<tr><td>keywords</td><td>sort, text, file</td></tr>
<tr><td>author</td><td>k4gvo -at- qsl.net (James Lynch)</td></tr>
<tr><td>maintained&nbsp;by</td><td>Eric Auer &lt;eric -at- coli.uni-sb.de&gt;</td></tr>
<tr><td>platforms</td><td>DOS (e.g. Borland Turbo C), includes own KITTEN library version, FreeDOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>GNU General Public License, Version 2</td></tr>
<tr><td>wiki&nbsp;site</td><td>http://wiki.freedos.org/wiki/index.php/Sort</td></tr>
</table>
